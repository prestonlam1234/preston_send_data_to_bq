#!pip install bittrex
from bittrex import Bittrex#, API_V2_0
import pandas as pd 
import requests
import json
from google.cloud import bigquery
import pandas_gbq


#my_bittrex = Bittrex(None, None)  # or defaulting to v1.1 as Bittrex(None, None)
#df = pd.DataFrame(my_bittrex.get_market_summary(market = 'BTC-XRP').json())

def get_bittrex_data(page):
	'''requests data from bittrex api and returns as dataframe'''
	df = pd.DataFrame(requests.get('https://bittrex.com/api/v1.1/public/' + page).json()['result'])
	return df


def convert_to_string(val):
	try:
		return str(val)
	except:
		return ''

#Try Job Config
def load_data_via_bq_client(page):
	df = get_bittrex_data(page)
	#convert all columns to string (maybe not best practice)
	for col in df.columns:
		df[col] = df[col].apply(lambda x: convert_to_string(x))
	#create schema from columns
	#documentation: https://cloud.google.com/bigquery/docs/samples/bigquery-load-table-dataframe
	schema_list = [bigquery.SchemaField(col, 'STRING', mode = 'NULLABLE') for 
		col in df.columns]
	print(schema_list)

	#create job
	job_config = bigquery.LoadJobConfig(
	    # Specify a (partial) schema. All columns are always written to the
	    # table. The schema is used to assist in data type definitions.
	    schema= schema_list,

	    write_disposition="WRITE_TRUNCATE",
	)

	dataset = "gcp-stl.preston_test_tables"
	table_id = dataset + '.bittrex_data_' + page

	job = client.load_table_from_dataframe(
	    df, table_id, job_config=job_config
	)  # Make an API request.
	job.result()  # Wait for the job to complete.

	table = client.get_table(table_id)  # Make an API request.
	print(
	    "Loaded {} rows and {} columns to {}".format(
	        table.num_rows, len(table.schema), table_id
	    )
	)

if __name__ == "__main__":


	client = bigquery.Client()

	print('successfully connected to client')
	
	pages = ['getmarkets', 'getcurrencies', 'getmarketsummaries']
	for page in pages:
		load_data_via_bq_client(page)

